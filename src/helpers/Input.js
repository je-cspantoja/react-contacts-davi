export class InputHelper {

  static bind_element(root, element_name, attribute_name) {
    var element = root.state[element_name];
    return (event) => {
      element[attribute_name] = event.target.value;
      root.setState({ [element_name]: element });
    }
  }

}