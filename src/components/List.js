import React from "react";
import { Component } from "react";

import Button from "components/Button";
import { ChildrenHelper } from "helpers/Children";

const DELETE_MESSAGE = "Você realmente quer deletar:";

export default class List extends Component {

  constructor(props) {
    super(props);
    this.state = { list: this.props.service.get_list() };
    this.props.service.register(this);
  }

  update(source, event) {
    this.setState({ list: this.props.service.get_list() });
  }

  delete(element) {
    let identifier = this.props.identifier;
    let service = this.props.service;
    if (window.confirm(`${DELETE_MESSAGE} ${element[identifier]}`)) {
      service.delete(element);
    }
  }

  render() {
    return (
      <ul>{
        this.state.list.map(
          element => <li key={element[this.props.identifier]}>
            {
              ChildrenHelper.instance_one(this, {element: element})
            }
            {
              this.props.deletable &&
              <Button
                handleClick={() => this.delete(element)}
                label="Remover"
              />
            }
          </li>
        )
      }</ul>
    )
  }
}